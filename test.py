import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

print("Hello World")
a = np.array([1, 2, 3, 4])
print(a)
df = pd.DataFrame({"date": ["Jan", "Feb", "Mac", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                   "book_sales": 2*np.arange(12) - 2 + 3*np.random.randn(12),
                   "stationary_sales": 0.5*np.arange(12) + 1 + 2*np.random.randn(12),
                   "cd_sales": 3*np.arange(12) + 3 + 3*np.random.randn(12)})
df.plot()
plt.show()
